# Type 地图切换

::: tip 提示
显示地图类型，单击可切换地图， 组件[API 文档](/api.html?url=/xdh-map/doc/module-xdh-map-type.html)
:::
## 基础用法

:::demo

```html
<template>
  <xdh-map>
    <xdh-map-type placement="right-bottom"></xdh-map-type>
  </xdh-map>
</template>
<script>
  import { XdhMap, XdhMapType } from 'xdh-map';

  export default {
    components: {
      XdhMap,
      XdhMapType
    }
  };
</script>
```

:::
